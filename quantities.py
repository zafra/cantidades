#!/usr/bin/env pythoh3

'''
Maneja un diccionario con cantidades
'''

"""
Programa creado por Alberto García Zafra
"""

import sys

items = {}


def op_add():
    """Add an item and quantity from argv to the items dictionary"""
    try:
        item = sys.argv[0]
        quantity = int(sys.argv[1])
    except ValueError:
        sys.exit("quantity must be an integer")

    if items.get(item):
        items[item] = quantity
    else:
        items.setdefault(item, quantity)


def op_items():
    """Print all items, separated by spaces"""
    listofitems = []
    if len(items) > 0:
        for item, quantity in items.items():
            listofitems.append(item)
        stringofitems = " ".join(listofitems)
        print(f"Items: {stringofitems}")
    else:
        print("There is no items added")


def op_all():
    """Print all items and quantities, separated by spaces"""
    listofitems2 = []
    if len(items) > 0:
        for item, quantity in items.items():
            listofitems2.append(f"{item} ({quantity})")
        stringofitems2 = " ".join(listofitems2)
        print(f"All: {stringofitems2}")
    else:
        print("There is no items added")


def op_sum():
    """Print sum of all quantities"""
    quantities = items.values()
    sum = 0
    for quantity in quantities:
        sum = sum + quantity

    print(f"Sum: {sum}")


def main():
    """
    Se hace un recorrido de todos los parámetros pasados por líneas de comandos,
    que se van guardando en la variable op, cuando op sea una operación,
    se llama a la función correspondente.
    """
    while sys.argv:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()


if __name__ == '__main__':
    main()
